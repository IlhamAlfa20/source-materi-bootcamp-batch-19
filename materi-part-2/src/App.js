import React from 'react';

import Routes from "./Materi-15/Routes"
import './App.css';
import {UserProvider} from "./Materi-17/UserContext"
import 'antd/dist/antd.css';

function App() {
  return (
    <>
      <UserProvider>
        <Routes/>
      </UserProvider>
    </>
  )
}

export default App;
