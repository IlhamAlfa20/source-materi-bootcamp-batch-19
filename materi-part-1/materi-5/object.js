// var person = {
//   firstName : "John",
//   lastName: "Doe",
//   gender: "male",
//   age: 27
// }

// person.age = 30;

// console.log(person)

// // console.log(person.firstname)
// // console.log(person["firstName"])

// var car = {
//   brand: "Ferrari",
//   type: "Sports Car",
//   price: 50000000,
//   "horse power": 986
// }

// car["horse power"] = 720

// console.log(car)

var mobil = [
  {merk: "BMW", warna: "merah", tipe: "sedan"}, 
  {merk: "toyota", warna: "hitam", tipe: "box"}, 
  {merk: "audi", warna: "biru", tipe: "sedan"}
]
//foreach
var mobil2 = mobil.forEach(
  function(item){
    console.log(item.warna)
  }
)

//map
var arrayWarna = mobil.map(function(item){
  return item.warna
})

console.log(arrayWarna)

var arrayMobilFilter = mobil.filter(function(item){
  return item.tipe != "sedan";
})

console.log(arrayMobilFilter)